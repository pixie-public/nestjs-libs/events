import { randomUUID } from 'crypto';
import { Message } from './models';
import { BadDecoratorArguments } from "./exceptions";

class SubscriberRefMap {
  readonly eventsSubscribed: Map<string, boolean>;
  readonly consumerEventsMap: Map<string, Map<string, any>>;
  readonly observations: (() => void)[];

  constructor() {
    this.consumerEventsMap = new Map<string, Map<string, any>>();
    this.eventsSubscribed = new Map<string, boolean>();
    this.observations = [];
  }

  observe(obs: () => void) {
    this.observations.push(obs);
  }

  notify() {
    this.observations.forEach(c => c());
  }

  getEventsSubscribed(): Map<string, boolean> {
    return this.eventsSubscribed;
  }

  register(handler, eventTypes, consumerKey) {
    eventTypes.forEach(t => {
      let typeName = t;
      if (typeof t !== 'string') {
        typeName = t.name;
      }
      this.eventsSubscribed.set(typeName, true);
    });

    let eventsSubscriberMap: Map<string, any[]>;

    if (this.consumerEventsMap.has(consumerKey)) {
      eventsSubscriberMap = this.consumerEventsMap.get(consumerKey);
    } else {
      eventsSubscriberMap = new Map<string, any[]>();
    }

    eventTypes.forEach(type => {
      let typeName = type;
      if (typeof type !== 'string') {
        typeName = type.name;
      }

      if (eventsSubscriberMap.has(typeName)) {
        eventsSubscriberMap.get(typeName).push(handler);
      } else {
        const list = [handler];
        eventsSubscriberMap.set(typeName, list);
      }
    });

    this.consumerEventsMap.set(consumerKey, eventsSubscriberMap);
    this.notify();
  }

  async propagate(consumerKey: string, msg: Message) {
    if (this.consumerEventsMap.has(consumerKey)) {
      const eventHandlersMap = this.consumerEventsMap.get(consumerKey);

      if (eventHandlersMap.has(msg.headers.kind)) {
        for (const subscriber of eventHandlersMap.get(msg.headers.kind)) {
          if (subscriber?.fn) {
            await subscriber.fn.apply(subscriber.target, [msg]);
          }
        }
      }

      if (eventHandlersMap.has(Message.name)) {
        for (const subscriber of eventHandlersMap.get(Message.name)) {
          if (subscriber?.fn) {
            await subscriber.fn.apply(subscriber.target, [msg]);
          }
        }
      }
    }

    if (consumerKey !== GENERIC_KEY) await this.propagate(GENERIC_KEY, msg);
  }
}

export const SUBSCRIBER_DECORATOR_REF = new SubscriberRefMap();
const GENERIC_KEY = randomUUID();

// Usage example:
//
// @Subscribe()
// @Subscribe('consumer')
// @Subscribe(['DummyEvent', DummyEvent, DummyEvent])
// @Subscribe([DummyEvent, 'DummyEvent', DummyEvent], 'consumerInjectionToken')
// async handleGeneric(e: Message) {
//   console.log('msg received:', e);
// }

const SubMethods = Symbol('SubMethods'); // just to be sure there won't be collisions

export function Subscribe(arg1?, arg2?) {
  return function(
    target: any,
    propertyKey: string,
    descriptor: PropertyDescriptor,
  ) {
    target[SubMethods] = target[SubMethods] || new Map();
    target[SubMethods].set(propertyKey, { arg1: arg1, arg2: arg2 });
  };
}

// eslint-disable-next-line @typescript-eslint/ban-types
export function Subscriber<T extends { new (...args: any[]): {} }>(Base: T) {
  return class extends Base {
    constructor(...args: any[]) {
      super(...args);
      const subMethods = Base.prototype[SubMethods];
      if (subMethods) {
        subMethods.forEach((props: any, method: string) => {
          _subscribe(this as any, method, props.arg1, props.arg2);
        });
      }
    }
  };
}

function _subscribe(target: any, propertyKey: string, arg1?, arg2?) {
  //consume all from all consumers
  if (arg1 === undefined && arg2 === undefined) {
    return handleSubscribeAll(target, propertyKey);
  }

  //all from one consumer
  if (
    (typeof arg1 === 'string' || arg1 instanceof String) &&
    arg2 === undefined
  ) {
    return handleSubscribeAll(target, propertyKey, arg1);
  }

  // specific event types from every consumer there is
  if (Array.isArray(arg1) && arg2 === undefined) {
    return handleSubscribeSpecific(target, propertyKey, arg1);
  }

  //specific events from specific consumer
  if (typeof arg2 === 'string' || arg2 instanceof String) {
    let types = arg1;
    if (!Array.isArray(arg1)) types = [arg1];

    return handleSubscribeSpecific(target, propertyKey, types, arg2);
  }

  throw new BadDecoratorArguments('Subscribe').withArgs(arg1, arg2);
}

function handleSubscribeSpecific(
  target,
  propertyKey,
  eventTypes,
  consumerKey?,
) {
  SUBSCRIBER_DECORATOR_REF.register(
    {
      fn: target[propertyKey],
      target,
    },
    eventTypes,
    consumerKey ? consumerKey : GENERIC_KEY,
  );
}

function handleSubscribeAll(target, propertyKey, consumerKey?) {
  return handleSubscribeSpecific(
    target,
    propertyKey,
    [Message],
    consumerKey ? consumerKey : GENERIC_KEY,
  );
}
