export class Base64 {
  public static decode(str: string): string {
    return Buffer.from(str, 'base64').toString('binary');
  }
  public static encode(str: string): string {
    return Buffer.from(str, 'binary').toString('base64');
  }
}
