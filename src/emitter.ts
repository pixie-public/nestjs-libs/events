import { EventMessage } from './models';
import { EventEmitter, Publisher } from './types';
import { Injectable } from '@nestjs/common';
import { Logger } from '@pixie-sh/logger';

@Injectable()
export class AbstractEventEmitter implements EventEmitter {
  constructor(private readonly injectionToken: string, private readonly publisher: Publisher = null) {
    if (publisher != null) {
      if (!EventEmitters.has(injectionToken)){
        EventEmitters.set(injectionToken, this);
      }
    } else {
      Logger.with('class', AbstractEventEmitter.name).error('provided publisher is null. ignored.');
    }
  }

  emit(eventMessage: EventMessage) {
    if (this.publisher === null) {
      Logger
          .with('headers', JSON.stringify(eventMessage.headers))
          .with('body', JSON.stringify(eventMessage.message()))
          .error('unable to emit message; publisher is null;');
      return;
    }

    if (eventMessage === null) {
      Logger
          .error('unable to emit null message;');
      return;
    }

    this.publisher.publish(eventMessage.message());
  }
}

export const EventEmitters: Map<string,EventEmitter> = new Map<string, EventEmitter>();
