import { KafkaConsumer, KafkaConsumerOptions, KafkaInstance, KafkaPublisher, KafkaPublisherOptions } from "../kafka";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { EventMessage, Message } from "../models";
import * as fs from "fs";
import * as dotenv from "dotenv";

class SomeMessage extends EventMessage {
  constructor() {
    super(SomeMessage.name);
  }

  a: number;
  b: string;
}

const sleep = ms => new Promise(r => setTimeout(r, ms));
const config = new ConfigService(dotenv.parse(fs.readFileSync('.env')))

const instance = new KafkaInstance(config, 'event.consumer');
const optionsConsumer = new KafkaConsumerOptions(config, 'event.consumer', instance);
const optionsProducer = new KafkaPublisherOptions(config, 'event.producer', instance);

const prod = new KafkaPublisher('SmKafkaPublisher', optionsProducer)
const consumer = new KafkaConsumer('SmKafkaConsumer', optionsConsumer);
consumer.onModuleInit().then(()=> {
  consumer.consume((ev: Message) => {
    console.log("[CONSUMER]", ev)
  }).then()
})

prod.onModuleInit().then(async () => {
  for (let i = 0; i < 0; i++) {
    const sm = new SomeMessage()
    sm.a = i;
    sm.b = `pub-idx-${i}`
    await prod.publish(sm)
    await sleep(200)
  }
})
