import { Injectable, NotImplementedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { Kafka, Consumer, Producer } from 'kafkajs';
import { InvalidKafkaConfiguration } from '../exceptions';
import { Base64 } from '../utils';
import {Logger} from "@pixie-sh/logger";

const sleep = ms => new Promise(r => setTimeout(r, ms));

@Injectable()
export class KafkaInstance {
  public kafka: Kafka;
  public readonly maxRetries: number;
  public readonly backOffTime: number;

  constructor(
    private configService: ConfigService,
    private configPrefix: string,
  ) {
    const brokers = configService
      .getOrThrow<string>(`${configPrefix}.kafka.brokers`)
      .split(',');
    const clientId = process.env.APP;
    if (clientId === undefined)
      throw new InvalidKafkaConfiguration(clientId).withFields(
          { key: "process.env.APP", value: "is required and not defined." });

    this.kafka = null;
    if (this.configService.get<string>(`${configPrefix}.kafka.withSSL`) === 'true') {
      this.kafka = new Kafka({
        brokers: brokers,
        clientId: clientId,
        ssl: {
          rejectUnauthorized:
            configService.get<string>(`${configPrefix}.kafka.ssl.rejectUnauthorized`) === 'true',
          ca: [
            Base64.decode(
                configService.getOrThrow<string>(`${configPrefix}.kafka.ssl.ca`),
            ),
          ],
          key: Base64.decode(
              configService.getOrThrow<string>(`${configPrefix}.kafka.ssl.key`),
          ),
          cert: Base64.decode(
              configService.getOrThrow<string>(`${configPrefix}.kafka.ssl.cert`),
          ),
        },
      });

    } else if (
      this.configService.get<string>(`${configPrefix}.kafka.withSASL`) === 'true'
    ) {
      this.kafka = new Kafka({
        brokers: brokers,
        clientId: clientId,
        ssl: true,
        sasl: {
          mechanism: configService.getOrThrow<string>(`${configPrefix}.kafka.sasl.mechanism`) as any,
          username: configService.getOrThrow<string>(`${configPrefix}.kafka.sasl.username`),
          password: configService.getOrThrow<string>(`${configPrefix}.kafka.sasl.password`)
        }
      });
    }

    if (!this.kafka) {
      throw new InvalidKafkaConfiguration(KafkaInstance.name).withFields({
        key: 'withSSL or withSASL',
        value: 'required parameters missing',
      });
    }


    this.maxRetries = this.configService.get<number>(
      `${configPrefix}.kafka.maxRetries`,
      5,
    );

    this.backOffTime = this.configService.get<number>(
      `${configPrefix}.kafka.backOffTimeMs`,
      2000,
    );
  }

  async connect(obj: Producer | Consumer): Promise<void> {
    for (let attempt = 0; attempt <= this.maxRetries; attempt++) {
      try {
        await obj.connect();
        return;
      } catch (error) {
        Logger.error(
          `error connecting to kafka brokers. attempt ${attempt + 1}/${
            this.maxRetries
          }: {0}`,
          error,
        );

        await sleep(attempt * this.backOffTime);
      }
    }
    throw new Error('Could not connect to kafka');
  }
}
