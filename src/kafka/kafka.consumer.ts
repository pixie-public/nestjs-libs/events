import { Injectable, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { SUBSCRIBER_DECORATOR_REF } from '../subscribe.decorator';
import { KafkaConsumerOptions } from './kafkaOptions.service';
import { Consumer, ConsumerHandler, ConsumerProcessingError, EventsMiddlewareInput, SCOPE } from "../";
import { Message } from '../models';
import { Logger, LoggerInterface} from '@pixie-sh/logger';
import { ConsumerSubscribeTopics, EachMessagePayload} from 'kafkajs';

@Injectable()
export class KafkaConsumer implements Consumer, OnModuleInit, OnModuleDestroy {
  private initialized: boolean;
  private consumers: ConsumerHandler[];
  private lastHeartbeatTime: number;
  private heatbeatTimerId: any;

  constructor(
    private consumerKey: string,
    private options: KafkaConsumerOptions,
  ) {
    this.initialized = false;
    this.consumers = [];
  }

  async onModuleInit(): Promise<void> {

    let subscribeConfig: ConsumerSubscribeTopics;
    if (this.options.topics && this.options.topics.length > 0) {
      subscribeConfig = { topics: this.options.topics, fromBeginning: this.options.fromBeginning }
    } else {
      subscribeConfig = { topics: [this.options.topic], fromBeginning: this.options.fromBeginning }
    }
    await this.options.consumer.subscribe(subscribeConfig);

    //do we have subscriptions already?
    if (SUBSCRIBER_DECORATOR_REF.consumerEventsMap.size > 0) {
      //looks like, start consuming
      await this.startConsuming();
    }

    SUBSCRIBER_DECORATOR_REF.observe(()=> {
      this.startConsumingOrNothing(this.options.instrumentConsumer);
    });
  }

  async onModuleDestroy(): Promise<void> {
    await this.options.consumer.disconnect();
  }

  consume(consumerHandler: ConsumerHandler): Promise<void> {
    this.consumers.push(consumerHandler);
    return this.startConsumingOrNothing(this.options.instrumentConsumer);
  }

  private async startConsumingOrNothing(instrumentConsumer: boolean = true): Promise<void> {
    if (this.initialized) return;
    if (instrumentConsumer) await this.setupInstrumentedEvents();

    await this.options.instance.connect(this.options.consumer);
    await this.startConsuming();
  }

  private async startConsuming() {
    this.initialized = true; //avoid new consumption task
    this.lastHeartbeatTime = Date.now();

    await this.options.consumer.run({
      autoCommit: false,
      eachMessage: async (em: EachMessagePayload) => {
        this.lastHeartbeatTime = Date.now();

        const topic = em.topic;
        const partition = em.partition;
        const message = em.message;

        // no scope and required by config
        let noGo = (process.env.SCOPE === undefined) && this.options.forceConsumeOnlyWithScope;
        // or no header scope and required by config
        noGo = noGo || (message.headers["SCOPE"] === undefined) && this.options.forceConsumeOnlyWithScope;
        // or the scopes do not match and is required by config
        noGo = noGo || message.headers["SCOPE"] !== undefined && message.headers["SCOPE"] != process.env.SCOPE && this.options.forceConsumeOnlyWithScope;

        if (noGo) {
          Logger.with('message.key', message.key.toString())
            .warn(
            `discarding message with scope {0} and expected scope {1} on partition/offset {2}/{3} topic {4}`,
              `${message.headers[SCOPE]}`,
              process.env.SCOPE,
              partition,
              message.offset,
              topic,
          );

          await this.commitOffset(topic, partition, message.offset);
          return;
        }

        try {
          const ev = JSON.parse(
            Buffer.from(message.value).toString(),
          ) as Message;

          if (!ev.headers.values) ev.headers.values = {}

          //set kafka message related info
          ev.headers.values['kafka'] = {
            timestamp: message.timestamp,
            offset: message.offset,
            partition,
            topic
          };

          //there was an issue with timestamp being inject before 10th March
          //force kafka timestamp if curren timestamp len is lower than 6digits
          if (ev.headers.timestamp && (ev.headers.timestamp.toString().length < 6 || !Number.isInteger(ev.headers.timestamp))) {
            Logger.warn('using kafka timestamp for {0}({1}) on partition/offset {2}/{3} topic {4}',
                ev.headers.kind,
                ev.headers.id,
                partition,
                message.offset,
                topic);

            ev.headers.timestamp = Number.parseInt(message.timestamp);
          }

          // middleware pre start
          const middlewareInput: EventsMiddlewareInput = {
            m: ev,
            data: {}
          }
          const middlewareOutputs = await Promise.all(this.options.middlewares.
            map(async mdw => {
              return await mdw.predicate(middlewareInput)
          }));

          const filteredMiddlewareOutputs = middlewareOutputs.filter(output => !output.result);
          if (filteredMiddlewareOutputs.length > 0) {
            Logger.with('message.key', message.key.toString())
              .warn(
                `middleware discarding message {0} with scope {1} on  topic {2} partition/offset {3}/{4}; explanation: {5}`,
                ev.headers.kind,
                `${message.headers[SCOPE]}`,
                topic,
                partition,
                message.offset,
                filteredMiddlewareOutputs.
                  map(output => output.result ? '' : output.explanation).
                  join(';')
              );

            await this.commitOffset(topic, partition, message.offset);
            return;
          }
          // middleware pre end

          // consume start
          await SUBSCRIBER_DECORATOR_REF.propagate(this.consumerKey, ev).catch(async (e) => {
            await this.handleConsumptionException(e, topic, partition, message, em)
          });
          this.consumers.forEach(c => c(ev));
          // consume end

          // middleware post start
          const middlewarePostOutputs = await Promise.all(this.options.middlewares.
          map(async mdw => {
            return await mdw.post(middlewareInput)
          }));

          if (middlewarePostOutputs.length) {
            const postLogger = Logger.with('middlewarePostOutputs.length', middlewarePostOutputs.length)
            for (let i1 = 0; i1 < middlewarePostOutputs.length; i1++){
              const postOutput = middlewarePostOutputs[i1];
                postLogger.with(`middlewarePostOutputs.${i1}`, postOutput)
            }

            postLogger.log(`middleware post outputs for ${ev.headers.id}`)
          } // middleware post end

          await this.commitOffset(topic, partition, message.offset);
        }
        catch (e) {
          await this.handleConsumptionException(e, topic, partition, message, em)
        }
      },
    });
  }

  private async commitOffset(topic, partition, offset) {
    return this.options.consumer.commitOffsets([{ topic, partition, offset: (Number(offset) + 1).toString() }]);
  }

  private async setupInstrumentedEvents() {
    this.options.consumer.on('consumer.connect', (event) => {
      const logger = Logger
        .with('connectEvent', event.id)
        .with('connectEventTimestamp', event.timestamp)
        .with('lastHeartBeat', this.lastHeartbeatTime)
        .with('heartbeatInterval', this.options.heartbeatIntervalMs)
        .with('heartbeatIntervalWatchdog', this.options.heartbeatIntervalWatchdogMs)
        .with('heartbeatIntervalCheck', this.options.heartbeatIntervalCheckMs);
        logger.log("kafka consumer connect event. starting heartbeat timer");

      this.heatbeatTimerId = setInterval(() => {
        const currentTime = Date.now();
        const elapsedTime = currentTime - this.lastHeartbeatTime;
        Logger.debug(`kafkajs heartbeat checkpoint now:${currentTime}; last beat: ${this.lastHeartbeatTime}; elapsed: ${elapsedTime}`)
        if (elapsedTime > this.options.heartbeatIntervalCheckMs) {
          Logger
              .with('lastHeartbeat', this.lastHeartbeatTime)
              .with('elapsedTime', elapsedTime)
              .error('kafka consumer has died, disconnected.');

          this.options.consumer.disconnect();
        }
      }, this.options.heartbeatIntervalWatchdogMs);

      logger.log("kafka consumer connect event. heartbeat created, timer id {0}", this.heatbeatTimerId);
    });

    this.options.consumer.on('consumer.disconnect', (event) => {
      Logger
          .with('disconnectEvent', event.id)
          .with('disconnectEventTimestamp', event.timestamp)
          .with('heartbeatTimeId', this.heatbeatTimerId).warn("kafka consumer disconnect event. clear timer and restart consumption.")
      if(this.heatbeatTimerId) clearInterval(this.heatbeatTimerId);

      this.initialized = false;
      this.startConsumingOrNothing(false);
    });

    this.options.consumer.on('consumer.stop', (event) => {
      Logger
        .with('stopEvent', event.id)
        .with('stopEventTimestamp', event.timestamp)
        .warn('kafkajs stop event. clear timer and restart consumption')

      if(this.heatbeatTimerId) clearInterval(this.heatbeatTimerId);

      this.initialized = false;
      this.options.consumer.disconnect();
    })

    this.options.consumer.on('consumer.crash', (event) => {
      Logger
        .with('crashEvent', event.id)
        .with('crashEventTimestamp', event.timestamp)
        .error('kafkajs crash event. clear timer and restart consumption')
      if(this.heatbeatTimerId) clearInterval(this.heatbeatTimerId);

      this.initialized = false;
      this.options.consumer.disconnect();
    })

    this.options.consumer.on('consumer.rebalancing', (event)=> {
      Logger
        .with('rebalanceEvent', event.id)
        .with('rebalanceEventTimestamp', event.timestamp)
        .log("kafkajs rebalancing")
    })

    this.options.consumer.on('consumer.heartbeat', (event) => {
      Logger
        .with('heartbeatEvent', event.id)
        .with('heartbeatEventTimestamp', event.timestamp)
        .debug("kafkajs heartbeat")
      this.lastHeartbeatTime = Date.now();
    });
  }

  async handleConsumptionException(e, topic, partition, message, em) {
    let exceptionLogger: LoggerInterface = Logger;
    exceptionLogger.with("trace", (e as any).stack);

    if (e instanceof ConsumerProcessingError) {
      exceptionLogger = exceptionLogger
        .with('exception', e.name)
        .with('message', e.message)
        .with('commitOffset', e.commitOffset);

      if (e.commitOffset) {
        exceptionLogger.warn(`committing partition/offset ${partition}/${message.offset}`)
        await this.commitOffset(topic, partition, message.offset);
      }
    }

    exceptionLogger.error(
      `error processing payload on topic ${topic}; offset/partition ${message.offset}/${em.partition}. exception: ${e}`,
    );
  }
}
