import { ConfigService } from '@nestjs/config';
import {
  KafkaConsumerOptions,
  KafkaConsumer,
  KafkaInstance,
  KafkaPublisher,
  KafkaPublisherOptions,
} from './';
import { AbstractEventEmitter } from '../emitter';
import { AbstractEventsMiddleware } from "../middlewares";
import { DefaultMiddlewaresInjectionKey, EmptyMiddlewaresInjectionKey } from "../types";

export function buildKafkaInstanceProvider(
  KAFKA_INSTANCE_INJECTION_KEY,
  optionsPrefix,
) {
  return [
    {
      provide: KAFKA_INSTANCE_INJECTION_KEY,
      useFactory: (config: ConfigService) => {
        return new KafkaInstance(config, optionsPrefix);
      },
      inject: [ConfigService],
    },
  ];
}

export function buildKafkaConsumerProvider(
  KAFKA_INSTANCE_INJECTION_KEY,
  INJECTION_KEY,
  optionsPrefix,
  middlewaresInjectionKey = EmptyMiddlewaresInjectionKey,
) {
  let providers = [
    {
    provide: INJECTION_KEY + 'Options',
    useFactory: (config: ConfigService, instance: KafkaInstance, middlewares: AbstractEventsMiddleware[]) => {
      return new KafkaConsumerOptions(config, optionsPrefix, instance, middlewares || []);
    },
    inject: [ConfigService, KAFKA_INSTANCE_INJECTION_KEY, middlewaresInjectionKey],
  },
  {
    provide: INJECTION_KEY,
    useFactory: (options: KafkaConsumerOptions) => {
      return new KafkaConsumer(INJECTION_KEY, options);
    },
    inject: [INJECTION_KEY + 'Options'],
  }];

  if(middlewaresInjectionKey === EmptyMiddlewaresInjectionKey) {
    return [
      {
        provide: middlewaresInjectionKey,
        useFactory: () => {
          return []
        },
        inject: [],
      },
      ...providers,
    ];
  }

  return providers
}

export function buildKafkaPublisherProvider(
  KAFKA_INSTANCE_INJECTION_KEY,
  INJECTION_KEY,
  optionsPrefix,
) {
  return [
    {
      provide: INJECTION_KEY + 'Options',
      useFactory: (config: ConfigService, instance: KafkaInstance) => {
        return new KafkaPublisherOptions(config, optionsPrefix, instance);
      },
      inject: [ConfigService, KAFKA_INSTANCE_INJECTION_KEY],
    },
    {
      provide: INJECTION_KEY,
      useFactory: (options: KafkaPublisherOptions) => {
        return new KafkaPublisher(INJECTION_KEY, options);
      },
      inject: [INJECTION_KEY + 'Options'],
    },
  ];
}

export const defaultEventEmitter = [
  ...buildKafkaInstanceProvider('KafkaInstance', 'event.producer'),
  ...buildKafkaPublisherProvider(
    'KafkaInstance',
    'EventProducer',
    'event.producer',
  ),
  {
    provide: 'DefaultEventEmitter',
    useFactory: (pub: KafkaPublisher) => {
      return new AbstractEventEmitter('DefaultEventEmitter', pub);
    },
    inject: ['EventProducer'],
  },
];
