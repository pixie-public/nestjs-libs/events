import { Injectable } from '@nestjs/common';
import {CompressionTypes, Consumer, Producer, Partitioners, ICustomPartitioner} from 'kafkajs';
import { ConfigService } from '@nestjs/config';
import { KafkaInstance } from '../';
import {Logger} from "@pixie-sh/logger";
import { AbstractEventsMiddleware } from "../middlewares";

export class KafkaServiceOptions {
  public topic: string;
  public readonly topics: string[];
  public readonly compression: CompressionTypes;

  constructor(
    protected configService: ConfigService,
    protected optionsPrefix: string,
    public readonly instance: KafkaInstance,
  ) {
    this.topic = undefined;
    this.topics = undefined;

    let topicsArr;
    const topicsStr = this.configService.get<string>(`${optionsPrefix}.kafka.topics`, "");
    if (topicsStr !== "") {
      Logger.log("KafkaServiceOptions topics is configured with {0} splitting by comma", topicsStr)
      topicsArr = topicsStr.split(',')
    }

    if (topicsArr && topicsArr.length > 1) {
      this.topics = topicsArr
    }

    if (topicsArr && topicsArr.length === 1) {
      Logger.log("KafkaServiceOptions topics is configured with {0} length {1}", topicsStr, topicsArr.length)
      this.topic = topicsArr[0]
    }

    if (!this.topics && !this.topic) {
      this.topic = this.configService.getOrThrow<string>(`${optionsPrefix}.kafka.topic`);
    }


    switch (
      this.configService.get<string>(
        `${optionsPrefix}.kafka.compression`,
        'NONE',
      )
    ) {
      case 'NONE':
        this.compression = CompressionTypes.None;
        break;
      case 'GZIP':
        this.compression = CompressionTypes.GZIP;
        break;
      case 'SNAPPY':
        this.compression = CompressionTypes.Snappy;
        break;
      case 'LZ4':
        this.compression = CompressionTypes.LZ4;
        break;
      case 'ZSTD':
        this.compression = CompressionTypes.ZSTD;
        break;
    }
  }
}

@Injectable()
export class KafkaPublisherOptions extends KafkaServiceOptions {
  public readonly producer: Producer;

  constructor(
    configService: ConfigService,
    optionsPrefix: string,
    instance: KafkaInstance,
  ) {
    super(configService, optionsPrefix, instance);

    if (!this.topic || this.topic === "") {
      this.topic = this.configService.getOrThrow<string>(`${optionsPrefix}.kafka.topic`);
    }

    const partitionerStr = this.configService.get(`${optionsPrefix}.kafka.producer.partitioner`, 'DEFAULT')

    let partitionerInterface: ICustomPartitioner = null
    switch(partitionerStr.toUpperCase()) {
      case 'LEGACY':
        partitionerInterface = Partitioners.LegacyPartitioner
      default:
        partitionerInterface = Partitioners.DefaultPartitioner
    }

    this.producer = this.instance.kafka.producer({ createPartitioner: partitionerInterface });
  }
}

@Injectable()
export class KafkaConsumerOptions extends KafkaServiceOptions {
  public readonly consumer: Consumer;
  public readonly fromBeginning: boolean;
  public readonly groupID: string;
  public readonly consumerGroupWithScope: boolean;
  public readonly forceConsumeOnlyWithScope: boolean;

  public readonly heartbeatIntervalMs: number;
  public readonly sessionTimeoutMs: number;
  public readonly instrumentConsumer: boolean;
  public readonly heartbeatIntervalCheckMs: number;
  public readonly heartbeatIntervalWatchdogMs: number;

  constructor(
    configService: ConfigService,
    optionsPrefix: string,
    instance: KafkaInstance,
    public readonly middlewares: AbstractEventsMiddleware[] = []
  ) {
    super(configService, optionsPrefix, instance);

    if(this.middlewares?.length) {
      Logger.log('KafkaConsumerOptions with middleware list of {0}: {1}', this.middlewares.length, this.middlewares.map(mdw => mdw.constructor.name).join(';'))
    }

    this.consumerGroupWithScope =
      this.configService.get<string>(`${optionsPrefix}.kafka.consumerGroupIDWithScope`, 'true') === 'true';

    this.forceConsumeOnlyWithScope =
      this.configService.get<string>(`${optionsPrefix}.kafka.forceConsumeOnlyWithScope`, 'true') === 'true';

    this.instrumentConsumer =
      this.configService.get<string>(`${optionsPrefix}.kafka.instrumentConsumer`, 'true') === 'true';

    this.groupID = this.configService.getOrThrow<string>(
      `${optionsPrefix}.kafka.groupId`,
    );

    if (process.env.SCOPE !== undefined && this.consumerGroupWithScope) {
      this.groupID = this.groupID + '_' + process.env.SCOPE;
    }

    this.fromBeginning = this.configService.getOrThrow<string>(`${optionsPrefix}.kafka.fromBeginning`) === 'true';

    this.heartbeatIntervalMs = this.configService.get<number>(`${optionsPrefix}.kafka.heartbeatIntervalMs`, 10000);
    this.heartbeatIntervalCheckMs = this.configService.get<number>(`${optionsPrefix}.kafka.heartbeatIntervalCheckMs`, this.heartbeatIntervalMs*1.25);
    this.heartbeatIntervalWatchdogMs = this.configService.get<number>(`${optionsPrefix}.kafka.heartbeatIntervalWatchdogMs`, this.heartbeatIntervalMs*0.90);
    this.sessionTimeoutMs = this.configService.get<number>(`${optionsPrefix}.kafka.sessionTimeoutMs`, this.heartbeatIntervalMs*3);

    this.consumer = this.instance.kafka.consumer({
      groupId: this.groupID,
      heartbeatInterval: this.heartbeatIntervalMs,
      sessionTimeout: this.sessionTimeoutMs,
    });
  }
}
