import { Injectable, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { KafkaPublisherOptions } from './kafkaOptions.service';
import { Publisher } from '../types';
import { Message } from '../models/message';
import { SCOPE } from '../';
import {Logger} from "@pixie-sh/logger";

@Injectable()
export class KafkaPublisher
  implements Publisher, OnModuleInit, OnModuleDestroy {
  constructor(
    private consumerKey: string,
    private options: KafkaPublisherOptions,
  ) {}

  async onModuleInit() {
    return this.options.instance.connect(this.options.producer);
  }

  async publish(message: Message) {
    try {
      const msgToSend = {
        key: message.headers.id,
        value: JSON.stringify(message),
      };

      if (process.env.SCOPE != undefined) {
        if (msgToSend['headers'] === undefined){
          msgToSend['headers'] = {};
        }

        msgToSend['headers'][SCOPE] = process.env.SCOPE;
      }

      await this.options.producer.send({
        topic: this.options.topic,
        compression: this.options.compression,
        messages: [msgToSend],
      });
    } catch (e) {
      Logger
          .with('payload', JSON.stringify(message))
          .with('topic', this.options.topic)
          .error('error sending message; {0}',  e);
    }
  }

  async onModuleDestroy(): Promise<void> {
    await this.options.producer.disconnect();
  }
}
