import { randomUUID } from 'crypto';
import { EventEmitters } from '../emitter';
import { Publisher } from "../types";
import { Logger } from "@pixie-sh/logger";

//abstract publisher consumer message
export class Message {
  public readonly headers: Headers;

  protected constructor(kind: string) {
    this.headers = new Headers(kind, randomUUID(), process.env.APP);
  }
}

export class Headers {
  public values: {
    [k: string]: any
  }

  constructor(
    public readonly kind: string,
    public id: string,
    public from: string,
    public timestamp: number = Date.now()
  ) {
    this.values = {};
  }
}

// event message, with event information
// meant to be specialized -> check PassGeneratedEvent <- for example imp
export abstract class EventMessage extends Message {
  private publishers: Publisher[];

  protected constructor(kind: string) {
    super(kind);
    this.publishers = null;
  }

  emit(forceUseDefaultEmitters = false) {
    if (this.publishers && this.publishers.length > 0) {
      Logger.with('eventType', this.headers.kind)
          .with('from', this.headers.from)
          .with('eventId', this.headers.id)
          .debug("using specific publisher.");

      const pub = this.publishers;
      delete this['publishers'];
      pub.forEach(p => p.publish(this));
      this.publishers = pub;

      if (!forceUseDefaultEmitters) return;
    }

    if (EventEmitters.size === 0) {
      Logger
          .with('eventType', this.headers.kind)
          .with('from', this.headers.from)
          .with('eventId', this.headers.id)
          .error('unable to emit event, no producer available');
      return;
    }


    //no custom publishers, custom publishers are empty, there's custom publishers and we want to force default emitter
    if (!this.publishers || this.publishers.length < 0 || this.publishers.length > 0 && forceUseDefaultEmitters){
      EventEmitters.forEach((ee, key) => ee.emit(this));
    }
  }

  message(): Message {
    return this;
  }

  withPublisher(...pub: Publisher[]): EventMessage {
    this.publishers = pub;
    return this;
  }

  withHeaderId(id: string): EventMessage {
    this.headers.id = id;
    return this;
  }

  withHeaderFrom(from: string): EventMessage {
    this.headers.from = from;
    return this;
  }

  withTimestamp(ts: number): EventMessage {
    this.headers.timestamp = ts;
    return this;
  }

}
