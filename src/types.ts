import { EventMessage, Message } from './models';

export const EmptyMiddlewaresInjectionKey = 'EmptyMiddlewaresInjectionKey'
export const DefaultMiddlewaresInjectionKey = 'DefaultMiddlewaresInjectionKey'

// message scope so specific consumers only consumes messages from specific producers
// for example:
// producer pr-81 -> scope will be pr-81;
// consumers from pr-82 will ignore them
export const SCOPE = 'SCOPE';

// this interface is meant to be used when a simple publisher is required.
// not actually related with event emitter
// check -> kafka folder <- for example impl
export interface Publisher {
  publish(eventMessage: Message);
}

//meant to be used when consumer is needed
// check -> kafka folder <- for example impl
export type ConsumerHandler = (ev: Message) => void;
export interface Consumer {
  consume(consumerHandler: ConsumerHandler): Promise<void>;
}

//this should be used when an event emitter is needed
// check -> emitters.ts  <- for concrete impl.
export interface EventEmitter {
  emit(eventMessage: EventMessage);
}
