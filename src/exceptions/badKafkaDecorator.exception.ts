export class BadDecoratorArguments implements Error {
  message: string;

  constructor(public name: string) {
    this.message = `${name} called with bad arguments;`;
  }

  withArgs(...args) {
    this.message += ` ${args}`;
    return this;
  }
}

export class ConsumerProcessingError implements Error {
  public message: string;
  public name: string = ConsumerProcessingError.name;

  constructor(public commitOffset: boolean, message: string = '') {
    this.message = `Error processing message; ${message}`;
  }

  withArgs(...args) {
    this.message += ` ${args}`;
    return this;
  }
}
