export class InvalidKafkaConfiguration implements Error {
  message: string;

  constructor(public name: string) {
    this.message = `Invalid configuration for ${name};`;
  }

  withFields(...args: { key: string; value: any }[]) {
    this.message += ` ${JSON.stringify(args)}`;
    return this;
  }
}
