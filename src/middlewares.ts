import { Message } from "./models";

export interface EventsMiddlewareOutput {
  result: boolean
  explanation?: string;
}
export interface EventsMiddlewareInput {
  m: Message;
  data: any;
}

export interface EventsMiddlewareInterface {
  predicate(m: EventsMiddlewareInput): Promise<EventsMiddlewareOutput>;
  post(m: EventsMiddlewareInput): Promise<EventsMiddlewareOutput>;
}

export abstract class AbstractEventsMiddleware implements EventsMiddlewareInterface{
  abstract predicate(m: EventsMiddlewareInput): Promise<EventsMiddlewareOutput>;
  abstract post(m: EventsMiddlewareInput): Promise<EventsMiddlewareOutput>;
}